package com.muraliveeravalli.threadsdemo.liveLock;

/**
 * Livelock -> Similar to deadlock but instead of threads being blocked, they are constantly active and waiting for other threads to complete their tasks
 * Since all threads are waiting for others to complete none of them can progress
 * Ex: Thread A loops till thread B complete its task & Thread B loops till Thread A completes its task -> Livelock
 *
 *
 * Slipped Condition -> it can occur when thread is suspended between reading the condition and acting on it.
 * Let us say we have 2 threads reading form buffer Each thread does the following
 *
 * 1) checks status
 * 2) if its status Ok reads data from buffer
 * 3) if data is EOF sets status to EOF and terminates, if data isin't EOF sets status to OK
 *
 * If sync is not happened properly following can happen
 * 1) Thread 1 checks status and gets Ok it suspends
 * 2) Thread 2 checks status Ok it reads EOF and reads data from buffer and sets status to EOF
 * 3) Thread 1 runs again it tries to read data ona oops there isint any data It crashes
 *
 * Solution to this is to use synchronized blocks / locks to execute critical section
 *
 * If code is already synchronized placement of synchronized can cause problems
 *
 * When using multiple threads the order in which the locks can be acquired can also result in slipped condition
 */
public class Main {
    public static void main(String[] args) {

        final Worker worker1 = new Worker("Worker 1", true);
        final Worker worker2 = new Worker("Worker 2", true);
        final SharedResource sharedResource = new SharedResource(worker1);

        new Thread(new Runnable() {
            @Override
            public void run() {
                worker1.work(sharedResource, worker2);
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                worker2.work(sharedResource, worker2);
            }
        }).start();

    }
}
