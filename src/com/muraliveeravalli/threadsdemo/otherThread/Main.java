package com.muraliveeravalli.threadsdemo.otherThread;

public class Main {

    /**
     * Atomic Action : When a threads running in can be suspended when its in middle of doing something
     * Ex: if thread is calling System.out.println it can be suspended in middle of executing the method, the value is evaluated but its suspended
     * before it can print the result
     * <p>
     * System.out.println is not a atomic action
     * <p>
     * Atomic action cant be suspended in middle of execution it either completes or it dosen't happen at all
     * Once a thread runs a atomic action we can be confident it wont be suspended util action is completed
     * <p>
     * Atomic Actions:
     * 1) reading and writing  reference variables myObj1 = myObj2
     * 2) Reading and writing primitive variables except long and double
     * JVM requires multiple operations to read and write long and double
     * myInt = 10 //atomic
     * myDouble = 1.2323 //not atomic
     * <p>
     * 3) Reading and writing all variables declared volatile
     * <p>
     * <p>
     * Volatile Variables:
     * When we use a non Volatile variable the JVM dosent guarentee when it writes an updated value back to memory
     * But when we use volatile Variable the JVM writes the value back to main memory immedieatly , It guarentees that every time
     * Thread reads form a volatile variable it gets the latest value
     *
     * To make a variable Volatile we use 'volatile' keyword
     *
     * You might be thinking we dont need to sync code that used volitile variable , Un fortunately that is not true
     * If only one thread is updating the value we dont need sync
     * But if multiple threads update variable we still get race condition
     *
     * Weather to sync when using a volitle variable will depend on code and what thread is doing
     * A common use of volitle is with variables of type long and double
     *
     * Reading and Writing longs & doubles isn't atomic
     *
     * Using volitle makes them atomic
     *
     * When only 1 thread can update value of shared variable then using volatile does mean we dont have to sync code
     * We can be  confident value in main memory is always latest value
     *
     * java.util.concurrency.atomic package provides us with classes that we can use to ensure reading and writing variable is atomic
     *
     */
    public static void main(String[] args) {

    }
}
