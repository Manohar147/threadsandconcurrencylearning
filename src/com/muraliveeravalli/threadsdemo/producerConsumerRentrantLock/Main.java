package com.muraliveeravalli.threadsdemo.producerConsumerRentrantLock;

import com.muraliveeravalli.threadsdemo.ThreadColor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Java introduced java.util.concurrent package in Java1.5
 * The classes and interfaces in this package are used to make it easy to work with multiple threads
 * <p>
 * We can avoid thread interfernce using the Lock interface
 * <p>
 * ReentrantLock is an alternative to synchronization in java
 * .lock() is used to lock a thread
 * <p>
 * When using Lock We are responsible to unlock the lock by calling .unlock()
 * <p>
 * We should always put the critical sections of code in try and finally block when using ReentrantLock
 * <p>
 * Using Try Finally is the best practise to use the ReentrantLock
 * <p>
 * A Thread can see if a lock is avaliable using the tryLock() method
 * If lock is available thread will acquire lock and continue
 * If lock is not available thread wont block and can execute some different code
 * <p>
 * Can also set timeOut to tryLock(2000);
 * ReentrantLock constructor accepts fairness parameter will give lock to thread waiting for longest
 * <p>
 * We can also check number of threads waiting for lock by calling getQueuedLink method
 */
public class Main {
    public static void main(String[] args) {
        List<String> buffer = new ArrayList<>();
        ReentrantLock bufferLock = new ReentrantLock();
        MyProducer producer = new MyProducer(buffer, ThreadColor.ANSI_YELLOW, bufferLock);
        MyConsumer consumer = new MyConsumer(buffer, ThreadColor.ANSI_PURPLE, bufferLock);
        MyConsumer consumerTwo = new MyConsumer(buffer, ThreadColor.ANSI_CYAN, bufferLock);
        new Thread(producer).start();
        new Thread(consumer).start();
        new Thread(consumerTwo).start();
    }
}

class MyProducer implements Runnable {
    private List<String> buffer;
    private String color;
    private ReentrantLock bufferLock;

    public MyProducer(List<String> buffer, String color, ReentrantLock reentrantLock) {
        this.buffer = buffer;
        this.color = color;
        this.bufferLock = reentrantLock;
    }

    @Override
    public void run() {
        Random random = new Random();
        String nums[] = {"1", "2", "3", "4", "5"};

        for (String num : nums) {
            try {
                System.out.println(color + "Adding..." + num);
                bufferLock.lock();
                try {
                    buffer.add(num);
                } finally {
                    bufferLock.unlock();
                }
                Thread.sleep(random.nextInt(2000));
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println(color + "Producer was Interrupted");
            }
        }
        System.out.println(color + "Adding EOF and exiting...");
        bufferLock.lock();
        try {
            buffer.add("EOF");
        } finally {
            bufferLock.unlock();
        }
    }
}

class MyConsumer implements Runnable {
    private List<String> buffer;
    private String color;
    private ReentrantLock bufferLock;


    public MyConsumer(List<String> buffer, String color, ReentrantLock reentrantLock) {
        this.buffer = buffer;
        this.color = color;
        this.bufferLock = reentrantLock;
    }

    @Override
    public void run() {
        int counter = 0;
        while (true) {
//            bufferLock.lock();
            if (bufferLock.tryLock()) { //using try lock
                try {
                    if (buffer.isEmpty()) {
                        continue;
                    }
                    System.out.println(color + "Counter: " + counter);
                    counter = 0;
                    if (buffer.get(0).equals("EOF")) {
                        System.out.println(color + "Exiting");
                        break;
                    } else {
                        System.out.println(color + "Removed " + buffer.remove(0));
                    }
                } finally {
                    bufferLock.unlock();
                }
            } else {
                //execute something else
                counter++;
            }
        }
    }
}
