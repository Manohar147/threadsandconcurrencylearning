package com.muraliveeravalli.threadsdemo.threadStarvation;

import com.muraliveeravalli.threadsdemo.ThreadColor;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Starvation -> it occurs when thread rarely have an oppourtunity to run and progress
 * So starvation occurs due to thread priority
 * <p>
 * When we used synchronize it is not first come first serve...</p>
 * <p>
 * there so guarentee that thread with highest priority will run first and thread with least priority will run last
 * <p>
 * It is not a first come first serve system
 * Because of this it can lead to Thread Starvation
 * <p>
 * FairLock -> Fair lock try to be first come first serve
 * ReentrantLock -> Allows us to create Fair locks
 * <p>
 * FairLock grantees only first come first serve ordering for getting the lock
 * Performance effects with fairLock
 * <p>
 * Livelock -> Similar to deadlock but instead of threads being blocked, they are constantly active and waiting for other threads to complete their tasks
 * Since all threads are waiting for others to complete none of them can progress
 * Ex: Thread A loops till thread B complete its task & Thread B loops till Thread A completes its task -> Livelock
 */
public class Main {
    //true means fair lock -> first come first serve
    private static ReentrantLock lock = new ReentrantLock(true);

    public static void main(String[] args) {
        Thread t1 = new Thread(new Worker(ThreadColor.ANSI_RED), "Priority 10");
        Thread t2 = new Thread(new Worker(ThreadColor.ANSI_BLUE), "Priority 8");
        Thread t3 = new Thread(new Worker(ThreadColor.ANSI_GREEN), "Priority 6");
        Thread t4 = new Thread(new Worker(ThreadColor.ANSI_CYAN), "Priority 4");
        Thread t5 = new Thread(new Worker(ThreadColor.ANSI_PURPLE), "Priority 2");
        t1.setPriority(10);
        t2.setPriority(8);
        t3.setPriority(6);
        t4.setPriority(4);
        t5.setPriority(2);

        t5.start();
        t3.start();
        t4.start();
        t1.start();
        t2.start();
    }

    private static class Worker implements Runnable {
        private int runCount = 1;
        private String threadColor;

        public Worker(String threadColor) {
            this.threadColor = threadColor;
        }

        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
//                synchronized (lock) {
                lock.lock();
                try {
                    System.out.println(threadColor + Thread.currentThread().getName() + " Run count = " + runCount++);
                    //execute critical section of code
                } finally {
                    lock.unlock();
                }
//                }
            }
        }
    }
}
