package com.muraliveeravalli.threadsdemo.deadlock;


/**
 * Deadlock -> Deadlock occurs when 2 or more threads are blocked on locks and every thread is holding lock of what another thread wants
 * Thread 1 has lock 1 and is waiting for lock 2
 * Thread 2 has lock 2 and is waiting for lock 1
 * If we run the code we see the code freezes and there is no output
 * How to prevent deadlock form happening
 * One way is to lock only on single object -> not particle
 * Second way to prevent deadlock is to write in such a way that all threads acquire lock in same order..
 * <p>
 * It is not possible for one of the threads to hold one lock and wait for another lock
 * Always try to obtain lock in same order to avoid deadlocks
 * <p>
 * Another solution is to use Lock Object rather than using synchronized lock
 */
public class DeadLock {

    private static Object lock1 = new Object();
    private static Object lock2 = new Object();

    public static void main(String[] args) {
        new Thread1().start();
        new Thread2().start();
    }

    public static class Thread1 extends Thread {

        @Override
        public void run() {
            synchronized (lock1) {
                System.out.println("Thread 1 has lock 1");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Thread 1 waiting for lock 2");
                synchronized (lock2) {
                    System.out.println("Thread 1 has lock 1 & lock 2");
                }
                System.out.println("Thread 1 released lock 2");
            }
            System.out.println("Thread 1 released lock 1 exiting");
        }
    }

    /**
     * Dead lock situation
     */
//    public static class Thread2 extends Thread {
//
//        @Override
//        public void run() {
//            synchronized (lock2) {
//                System.out.println("Thread 2 has lock 2");
//                try {
//                    Thread.sleep(2000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                System.out.println("Thread 2 waiting for lock 1");
//                synchronized (lock1) {
//                    System.out.println("Thread 2 has lock 2 & lock 1");
//                }
//                System.out.println("Thread 2 released lock 1");
//            }
//            System.out.println("Thread 2 released lock 2");
//        }
//    }

    /**
     * Getting lock in same order
     */
    public static class Thread2 extends Thread {

        @Override
        public void run() {
            synchronized (lock1) {
                System.out.println("Thread 2 has lock 1");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Thread 2 waiting for lock 1");
                synchronized (lock2) {
                    System.out.println("Thread 2 has lock 2 & lock 1");
                }
                System.out.println("Thread 2 released lock 2");
            }
            System.out.println("Thread 2 released lock 1");
        }
    }

}
