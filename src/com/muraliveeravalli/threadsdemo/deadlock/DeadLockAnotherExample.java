package com.muraliveeravalli.threadsdemo.deadlock;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class DeadLockAnotherExample {

    private static int lock1;
    private static int lock2;
    private static ReentrantLock reentrantLock = new ReentrantLock(true);
    private static Condition condition = reentrantLock.newCondition();

    public static void main(String[] args) {
        new Thread1().start();
        new Thread2().start();
    }

    public static class Thread1 extends Thread {

        @Override
        public void run() {
            System.out.println("Thread 1 Obtaining a lock..." + lock1 + "," + lock2);
            reentrantLock.lock();
            System.out.println("Thread 1 lock obtained :),incrementing lock1");
            lock1++;
            System.out.println("Awaiting conformation from Thread 2");
            try {
                condition.await();
                System.out.println("Proceding....");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread 1 : Condition Confirmed by Thread 2, Thread 1 contineous to execute...");
            try {
                System.out.println("Thread 1 sleeping for 2 seconds notifying thread 2 to continue");
                reentrantLock.unlock();
                Thread.sleep(5000);
                System.out.println("Thread 1 woke up waiting for conformation from thread 2");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                System.out.println("Thread 1 woke up trying to lock again");
                reentrantLock.lock();
                System.out.println("Thread 1 lock acquired .... again awaiting conformation from thread 2");
                System.out.println("Thread 2 conformation acquired");
            } catch (Exception e) {
                e.printStackTrace();
            }
            reentrantLock.lock();
            System.out.println("Thread ,incrementing lock1 and lock 2");
            lock2++;
            lock1++;
            reentrantLock.unlock();
            System.out.println("Thread 1 released lock");
            reentrantLock.unlock();
            System.out.println("Thread 1 released lock all lock finished execution");
            System.out.println("Thread 1 final result: " + lock1 + "," + lock2);
        }
    }

    public static class Thread2 extends Thread {

        @Override
        public void run() {
            System.out.println("Thread 2 Obtaining a lock..." + lock1 + "," + lock2);
            reentrantLock.lock();
            lock1--;
            System.out.println("Thread 2 lock obtained :),decrementing lock1 : " + lock1);
            try {
                System.out.println("Thread 2 sleeping for 2 seconds giving lock to thread 1");
                condition.signalAll();
                reentrantLock.unlock();
                Thread.sleep(2000);
                System.out.println("Thread 2 woke up locking again");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            reentrantLock.lock();
            System.out.println("Thread 2 waiting for lock 1");
            reentrantLock.lock();
            System.out.println("Thread 2,decrementing lock1 and lock 2");
            lock2--;
            lock1--;
            System.out.println("Notifying Thread 1 to continue");
            reentrantLock.unlock();
            System.out.println("Thread 2 released lock ");
            reentrantLock.unlock();
            System.out.println("Thread 2 released lock all lock finished execution");
            System.out.println("Thread 2 final result: " + lock1 + "," + lock2);
        }
    }


}

