package com.muraliveeravalli.threadsdemo.challenge;

/**
 * Create and Start Threads
 * <p>
 * We could have 2 people using a join bank Account at same time.Create and start 2 threads that use same Bank Account
 * instance and an initial bal of 1000 One will deposit 300 and then withdraw 50
 * Other will deposit 203.75 and withdraw 100
 */
public class Main {
    public static void main(String[] args) {
        final BankAccount account = new BankAccount(1000, "12345-678");

        new Thread(() -> {
            account.deposit(300);
            account.withdraw(50);

            System.out.println("T1:" + account);
        }).start();

        new Thread(() -> {
            account.deposit(203.75);
            account.withdraw(100);
            System.out.println("T2:" + account);
        }).start();
    }
}
