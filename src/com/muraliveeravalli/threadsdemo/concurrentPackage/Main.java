package com.muraliveeravalli.threadsdemo.concurrentPackage;

import com.muraliveeravalli.threadsdemo.ThreadColor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Java introduced java.util.concurrent package in Java1.5
 * The classes and interfaces in this package are used to make it easy to work with multiple threads
 *
 * We can avoid thread interfernce using the Lock interface
 *
 *
 *
 *
 */
public class Main {
    public static void main(String[] args) {
        List<String> buffer = new ArrayList<>();
        MyProducer producer = new MyProducer(buffer, ThreadColor.ANSI_YELLOW);
        MyConsumer consumer = new MyConsumer(buffer, ThreadColor.ANSI_PURPLE);
        MyConsumer consumerTwo = new MyConsumer(buffer, ThreadColor.ANSI_CYAN);
        new Thread(producer).start();
        new Thread(consumer).start();
        new Thread(consumerTwo).start();
    }
}

class MyProducer implements Runnable {
    private List<String> buffer;
    private String color;

    public MyProducer(List<String> buffer, String color) {
        this.buffer = buffer;
        this.color = color;
    }

    @Override
    public void run() {
        Random random = new Random();
        String nums[] = {"1", "2", "3", "4", "5"};

        for (String num : nums) {
            try {
                System.out.println(color + "Adding..." + num);
                synchronized (buffer) {
                    buffer.add(num);
                }
                Thread.sleep(random.nextInt(2000));
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println(color + "Producer was Interrupted");
            }
        }
        System.out.println(color + "Adding EOF and exiting...");
        synchronized (buffer) {
            buffer.add("EOF");
        }
    }
}

class MyConsumer implements Runnable {
    private List<String> buffer;
    private String color;

    public MyConsumer(List<String> buffer, String color) {
        this.buffer = buffer;
        this.color = color;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (buffer) {
                if (buffer.isEmpty()) {
                    continue;
                }
                if (buffer.get(0).equals("EOF")) {
                    System.out.println(buffer);
                    System.out.println(color + "Exiting");
                    break;
                } else {
                    System.out.println(color + "Removed " + buffer.remove(0));
                }
            }
        }

    }
}
