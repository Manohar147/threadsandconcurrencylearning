package com.muraliveeravalli.threadsdemo.moreOnDeadLocks;

/**
 * Will become a deadlock
 * How can there be deadlock
 */
public class Main {

    public static void main(String[] args) {
        final PolitePerson jane = new PolitePerson("Jane");
        final PolitePerson jhon = new PolitePerson("Jhon");
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                jane.sayHello(jhon);
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                jhon.sayHello(jane);
            }
        });
        t1.start();
        t2.start();
    }

    static class PolitePerson {
        private final String name;

        public PolitePerson(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public synchronized void sayHello(PolitePerson person) {
            System.out.println("got lock on " + this.getName());
            System.out.format("%s" + " has said hello to %s%n", this.name, person.getName());
            System.out.println("calling  " + person.getName());
            person.sayHelloBack(this);
        }

        public synchronized void sayHelloBack(PolitePerson person) {
            System.out.format("%s" + " has said hello back to %s%n", this.name, person.getName());

        }
    }
}
