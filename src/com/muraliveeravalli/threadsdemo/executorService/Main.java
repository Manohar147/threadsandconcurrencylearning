package com.muraliveeravalli.threadsdemo.executorService;

import com.muraliveeravalli.threadsdemo.ThreadColor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.locks.ReentrantLock;

/**
 * ExecutorService is found in java.util.concurrent package
 * We use this to manage threads for us so that we dont have to create and start threads
 * They optimize creation of threads
 * We must provide Runnable Objects to the service
 * <p>
 * ExecutorService impl makes use of Thread Pools
 * A Thread pool is managed set of threads and reduces the overhead of creating threads
 * <p>
 * We can also decide how many thread to be active
 * <p>
 * .execute(someRunnable) is the method which is alternative to new Thread(someRunnable).start();
 * <p>
 * Call .shutdown() to terminate the ExecutorService will wait for any tasks to terminate
 * Call .shutdownNow() will shut down immediately
 * <p>
 * ExecutorService It is vital for apps with large number of threads
 * <p>
 * If we want a data back form thread we can use the submit() method
 * submit accepts a callable Object value can be returned as an object of type Future
 */
public class Main {
    public static void main(String[] args) {
        List<String> buffer = new ArrayList<>();
        ReentrantLock bufferLock = new ReentrantLock();
        MyProducer producer = new MyProducer(buffer, ThreadColor.ANSI_YELLOW, bufferLock);
        MyConsumer consumer = new MyConsumer(buffer, ThreadColor.ANSI_PURPLE, bufferLock);
        MyConsumer consumerTwo = new MyConsumer(buffer, ThreadColor.ANSI_CYAN, bufferLock);
        //create room for 5 active threads
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        executorService.execute(producer);
        executorService.execute(consumer);
        executorService.execute(consumerTwo);

        Future<String> future = executorService.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return "This is the callable result";
            }
        });
        try {
            // Will block until result is available
            System.out.println(future.get());
        } catch (ExecutionException | InterruptedException e) {
            System.out.println("Something went wrong");
        }
        System.out.println("Calling shutDown");
        executorService.shutdown();
    }
}

class MyProducer implements Runnable {
    private List<String> buffer;
    private String color;
    private ReentrantLock bufferLock;

    public MyProducer(List<String> buffer, String color, ReentrantLock reentrantLock) {
        this.buffer = buffer;
        this.color = color;
        this.bufferLock = reentrantLock;
    }

    @Override
    public void run() {
        Random random = new Random();
        String nums[] = {"1", "2", "3", "4", "5"};

        for (String num : nums) {
            try {
                System.out.println(color + "Adding..." + num);
                bufferLock.lock();
                try {
                    buffer.add(num);
                } finally {
                    bufferLock.unlock();
                }
                Thread.sleep(random.nextInt(2000));
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println(color + "Producer was Interrupted");
            }
        }
        System.out.println(color + "Adding EOF and exiting...");
        bufferLock.lock();
        try {
            buffer.add("EOF");
        } finally {
            bufferLock.unlock();
        }
    }
}

class MyConsumer implements Runnable {
    private List<String> buffer;
    private String color;
    private ReentrantLock bufferLock;


    public MyConsumer(List<String> buffer, String color, ReentrantLock reentrantLock) {
        this.buffer = buffer;
        this.color = color;
        this.bufferLock = reentrantLock;
    }

    @Override
    public void run() {
        int counter = 0;
        while (true) {
//            bufferLock.lock();
            if (bufferLock.tryLock()) { //using try lock
                try {
                    if (buffer.isEmpty()) {
                        continue;
                    }
                    System.out.println(color + "Counter: " + counter);
                    counter = 0;
                    if (buffer.get(0).equals("EOF")) {
                        System.out.println(color + "Exiting");
                        break;
                    } else {
                        System.out.println(color + "Removed " + buffer.remove(0));
                    }
                } finally {
                    bufferLock.unlock();
                }
            } else {
                //execute something else
                counter++;
            }
        }
    }
}


