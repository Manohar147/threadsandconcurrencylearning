package com.muraliveeravalli.threadsdemo.multipleThreads.counter;


import com.muraliveeravalli.threadsdemo.ThreadColor;

/**
 * <p>By sharing the same resource in multiple threads
 * Heap is the application memory where all threads share
 * All threads have thread stack which other threads cannot share but all threads can share the heap
 * Local variables are in thread stack
 * But Objects fields are stored in application Heap
 * And thus can cause thread interference</p>
 * <p>
 * Thread interference happens when multiple threads try to write a same resource
 * </p>
 * This is also called as race-condition, No problem in reading the resource, only problem is when one of the thread tries to write to a resource
 * One way to avoid this interference is to create two different CountDown instances
 * But this solution wont work in real world application...
 * <p>
 * Fortunately java provides a easy way for a thread to change value in the heap
 * this process is called "synchronization"
 * By using synchronization if a thread is executing a synchronization code all other threads must wait till the current thread has completed
 * <p>
 * This prevents thread interference
 * When working with threads we have to synchronization all the places where we think thread interference can happen
 * example -> public synchronized void doCountDown() {}
 * synchronized on methods says the object where the method is used is the resource that is synchronized..
 * synchronized cannot be used on constructors
 * <p>
 * We can also synchronized block of statements instead of an entire method
 * Every Object has a monitor by using synchronized the thread acquires a thread monitor before executing synchronized block
 * Only one thread can hold a lock at time others threads are suspended till the current thread released the lock
 * synchronized (this) { -> CountDown instance is synchronized
 * for (i = 10; i > 0; i--) {
 * System.out.println(color + Thread.currentThread().getName() + ": i = " + i);
 * }
 * }
 * <p>
 * Do not synchronized local variables ie variables stored in thread stack
 * Use synchronized on shared resources
 * You can also synchronize static methods and objects ..
 * When synchronizing static methods and objects by doing this the lock is owned by the class object associated with the object class
 */
public class Main {
    public static void main(String[] args) {

//        CountDown countDown1 = new CountDown();
//        CountDown countDown2 = new CountDown();
        CountDown countDown = new CountDown();
        CountDownThread t1 = new CountDownThread(countDown);
        t1.setName("Thread 1");
        CountDownThread t2 = new CountDownThread(countDown);
        t2.setName("Thread 2");
        t1.start();
        t2.start();
    }
}

class CountDown {

    /*
    Both threads sharing the same resource
    stored in heap
     */
    private int i;

    //    public synchronized void doCountDown()
    public void doCountDown() {
        // stored in stack
        String color;
        switch (Thread.currentThread().getName()) {
            case "Thread 1":
                color = ThreadColor.ANSI_CYAN;
                break;
            case "Thread 2":
                color = ThreadColor.ANSI_PURPLE;
                break;
            default:
                color = ThreadColor.ANSI_GREEN;
        }
        synchronized (this) {
            for (i = 10; i > 0; i--) {
                System.out.println(color + Thread.currentThread().getName() + ": i = " + i);
            }
        }
    }
}

class CountDownThread extends Thread {
    private CountDown threadCountdown;

    public CountDownThread(CountDown countDown) {
        this.threadCountdown = countDown;
    }

    @Override
    public void run() {
        threadCountdown.doCountDown();
    }
}
