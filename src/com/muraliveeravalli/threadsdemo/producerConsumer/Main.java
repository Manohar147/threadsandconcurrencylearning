package com.muraliveeravalli.threadsdemo.producerConsumer;

import java.util.Random;

/**
 * Locking is re entrant i.e a Thread can acquire a lock it already owns
 * <p>
 * Critical Section -> refers to code that has a shared resource
 * Only one thread at a time should be able to execute critical section
 * <p>
 * Thread safe -> A class is thread safe if all the code within the class is synchronized
 * When we are synchronizing code we must only do them where the code must be synchronized
 * unnecessary synchronization can cost performance
 * <p>
 * Methods That can be called in synchronized code
 * wait(), notify(), notifyAll() methods...
 * <p>
 * synchronizing both methods ensures that when a read is invoked write is not invoked and vice versa
 * <p>
 * wait(),notify(),notifyAll()
 * When a thread calls the wait(); it releases the lock on the object and waits till another thread calls notify/notifyAll
 * Always call wait() in loops so that when it returns we will go back to beginning
 * <p>
 * We cant notify a specific thread
 * notify() -> Notify only single thread
 * Atomic Operations (Where thread cant be suspended):
 * 1) Reading and writing reference variables
 * myOb1 = myObj2 will be atomic;
 * 2) Reading and Writing primitive variables except long & doubles
 * int myInt = 10; cant be suspended
 * double myDouble  = 1.23232 can be suspended
 * <p>
 * Some collections aren't Thread safe
 * ArrayList is not thread safe we can call Collections.synchronizeList(arrayList)
 * Vector is thread safe i.e code for vector is synchronized
 */
public class Main {

    public static void main(String[] args) {
        Message message = new Message();

        Writer writer = new Writer(message);
        Reader reader = new Reader(message);

        new Thread(writer).start();
        new Thread(reader).start();

    }
}

class Message {
    private String message;
    private boolean empty = true;

    /**
     * Will be used by consumer to read the message
     * empty -> true when there is no message to read
     *
     * @return message
     */
    public synchronized String read() {
        System.out.println("In read() empty: " + empty);
        while (empty) {
            try {
                System.out.println("Called wait() on read()");
                wait();
                System.out.println("Waiting.. Finished Reader");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        empty = true;
        System.out.println("Notifying Writer");
        notifyAll();
        return message;
    }

    /**
     * Will me used by producer to write a message
     * empty -> false when there message to read
     *
     * @param message
     */
    public synchronized void write(String message) {
        System.out.println("In write() empty: " + empty);
        while (!empty) {
            try {
                System.out.println("Called wait() on write()");
                wait();
                System.out.println("Waiting.. Finished Writer");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        empty = false;
        this.message = message;
        System.out.println("Notifying Reader");
        notifyAll();
    }
}

class Writer implements Runnable {
    private Message message;

    public Writer(Message message) {
        this.message = message;
    }

    @Override
    public void run() {
        String messages[] = {
                "Humpty Dumpty Sat on a wall",
                "Humpty Dumpty had a great fall",
                "All the kings horses and all the kings men",
                "Couldn't put humpty together again"
        };
        Random random = new Random();
        for (int i = 0; i < messages.length; i++) {
            message.write(messages[i]);
            try {
                Thread.sleep(random.nextInt(2000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        message.write("Finished");
    }
}

class Reader implements Runnable {
    private Message message;

    public Reader(Message message) {
        this.message = message;
    }

    @Override
    public void run() {
        Random random = new Random();
        for (String latestMessage = message.read(); !latestMessage.equals("Finished"); latestMessage = message.read()) {
            System.out.println("Latest Message: " + latestMessage);
            try {
                Thread.sleep(random.nextInt(5000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
