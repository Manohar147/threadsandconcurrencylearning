package com.muraliveeravalli.threadsdemo.threadsBasics;

public class MainNew {


    public static void main(String[] args) {
        SomeClass someClass = new SomeClass();
        Thread thread = new Thread(new MyClass(someClass));
        thread.start();
        Thread thread1 = new Thread(new MyOtherClass(someClass));
        thread1.start();
    }

}

class SomeClass {

    public synchronized void doThis() throws InterruptedException {
        System.out.println("in Do this waiting" + Thread.currentThread().getName());
        Thread.sleep(5000);
    }

    public synchronized void doThat() throws InterruptedException {
        System.out.println("in Do That");
        doThis();
    }
}

class MyClass implements Runnable {
    SomeClass someClass;

    public MyClass(SomeClass someClass) {
        this.someClass = someClass;
    }

    @Override
    public void run() {
        try {
            someClass.doThis();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class MyOtherClass implements Runnable {
    SomeClass someClass;

    public MyOtherClass(SomeClass someClass) {
        this.someClass = someClass;
    }

    @Override
    public void run() {
        try {
            someClass.doThat();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
